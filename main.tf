provider "aws" {
  region     = var.region
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_default_security_group" "default" {
  vpc_id = "${aws_default_vpc.default.id}"

  ingress {
    protocol  = -1
    self      = true
    from_port = 0
    to_port   = 0
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_default_subnet" "default_az1" {
  availability_zone = "eu-central-1a"

  tags = {
    Name = "Default subnet for eu-central-1a"
  }
}

resource "aws_default_subnet" "default_az2" {
  availability_zone = "eu-central-1b"

  tags = {
    Name = "Default subnet for eu-central-1b"
  }
}

resource "aws_default_subnet" "default_az3" {
  availability_zone = "eu-central-1c"

  tags = {
    Name = "Default subnet for eu-central-1c"
  }
}

resource "aws_api_gateway_rest_api" "api_gateway" {
  name = "api"
}

resource "aws_rds_cluster" "postgresql" {
  engine_mode             = "serverless"
  availability_zones      = ["eu-central-1a", "eu-central-1b", "eu-central-1c"]
  database_name           = "${var.database_name}"
  master_username         = "${var.database_user}"
  master_password         = "${var.database_pass}"
  backup_retention_period = 5
  preferred_backup_window = "07:00-09:00"
  skip_final_snapshot = true
}

module "crud_backend" {
  source = "./modules/crud_backend"
  api_gateway="${aws_api_gateway_rest_api.api_gateway}"
  security_group="${aws_default_security_group.default}"
  subnets= ["${aws_default_subnet.default_az1.id}","${aws_default_subnet.default_az2.id}","${aws_default_subnet.default_az3.id}"]
  db_host="${aws_rds_cluster.postgresql.endpoint}"
  db_name="${var.database_name}"
  db_user="${var.database_user}"
  db_pass="${var.database_pass}"
}

module "website" {
  source = "./modules/website"
  apiUrl = "${module.crud_backend.apiUrl}"
}

output "website-url" {
  value = "${module.website.website-url}"
}
