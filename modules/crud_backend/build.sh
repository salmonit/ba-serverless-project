#!/bin/bash
cd "$(dirname "$0")"

build() {
    mkdir -p src/bin
    cd src/bin
    go build ../insert
    go build ../list
    go build ../delete
}

echo "Building"
build
