package main

import (
	"context"
	"fmt"
	"time"
	"github.com/liamylian/jsontime"
	"moritz31/meal-lambda/database"
	"moritz31/meal-lambda/models"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/jinzhu/gorm"
)

var db *gorm.DB
var json = jsontime.ConfigWithCustomTimeFormat


type Meal struct {
  Date string `json:"date"`
  Name string `json:"name"`
}

func init() {
	db = database.Open()
}

func main() {
	lambda.Start(Handler)
}

func Handler(ctx context.Context, req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	// You can use db here.

	db.AutoMigrate(&models.Meal{})

	date := fmt.Sprintf("%s-%s-%s", 
		req.PathParameters["year"],
		req.PathParameters["month"],
		req.PathParameters["day"])
	_, err := time.Parse("2006-1-2", date)
	if err != nil {
		fmt.Printf("Error: Not provided valid date")
		return events.APIGatewayProxyResponse{
			StatusCode: 500,
			Headers: map[string]string {
				"Access-Control-Allow-Origin" : "*",
				"Accces-Control-Allow-Headers" : "*",
			},
			Body: "Not a valid date",
		}, nil
	}
	fmt.Println(date)

	meal:= &models.Meal{Date: date}
	db.Delete(&meal)

	return events.APIGatewayProxyResponse{
		StatusCode: 200,
		//Body: fmt.Sprintf("%s",result.Error),
	}, nil
}
