package main

import (
	"context"
	"fmt"
	"time"
	"github.com/liamylian/jsontime"
	"moritz31/meal-lambda/database"
	"moritz31/meal-lambda/models"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/jinzhu/gorm"
)

var db *gorm.DB
var json = jsontime.ConfigWithCustomTimeFormat


type Meal struct {
  Date string `json:"date"`
  Name string `json:"name"`
}

func init() {
	db = database.Open()
}

func main() {
	lambda.Start(Handler)
}

func Handler(ctx context.Context, req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	// You can use db here.

	fmt.Println(req.Body)

	var inputMeal models.Meal
	err := json.Unmarshal([]byte(req.Body), &inputMeal)

	_, err = time.Parse("2006-1-2", inputMeal.Date)
	if err != nil {
		fmt.Printf("Error: Not provided valid date")
		return events.APIGatewayProxyResponse{
			StatusCode: 500,
			Headers: map[string]string {
				"Access-Control-Allow-Origin" : "*",
				"Accces-Control-Allow-Headers" : "*",
			},
			Body: "Not a valid date",
		}, nil
	}

	db.AutoMigrate(&models.Meal{})

	meal := &models.Meal{}
	meal.Date = inputMeal.Date
	meal.Name = inputMeal.Name
	db.Create(meal)
	  
	return events.APIGatewayProxyResponse{
		StatusCode: 200,
		Headers: map[string]string {
			"Access-Control-Allow-Origin" : "*",
			"Accces-Control-Allow-Headers" : "*",
		},
		//Body: fmt.Sprintf("%s %s",meal.Date,meal.Meal),
	}, nil
}
