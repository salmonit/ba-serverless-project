package models

import "github.com/jinzhu/gorm"

type Meal struct {
   gorm.Model
   Date string `json:"date" gorm:"primary_key;unique;not null"`
   Name  string `json:"name"`
   Price  string `json:"price"`
}