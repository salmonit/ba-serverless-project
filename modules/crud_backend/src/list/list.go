package main

import (
	"github.com/liamylian/jsontime"
	"fmt"
	"moritz31/meal-lambda/models"
	"moritz31/meal-lambda/database"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/jinzhu/gorm"
)

var db *gorm.DB
var json = jsontime.ConfigWithCustomTimeFormat

type Meal struct {
  gorm.Model
  Date string `gorm:"sql=unique"`
  Meal string
}

type queryMonth struct {
	Month string
	Year string 
}

func init() {
	db = database.Open()
}

func main() {
	lambda.Start(Handler)
}

func Handler(event events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	// You can use db here.
	db.AutoMigrate(&models.Meal{})

	meals := []models.Meal{}
	
	if result := db.Where("MONTH(date) = ? AND YEAR(date) = ?",event.PathParameters["month"], event.PathParameters["year"]).Find(&meals); result.Error != nil {
		return events.APIGatewayProxyResponse{
			StatusCode: 500,
			Body: fmt.Sprintf("%s",result.Error),
		}, nil
	} 

	return events.APIGatewayProxyResponse{
		StatusCode: 201,
		Headers: map[string]string {
			"Access-Control-Allow-Origin" : "*",
		},
		Body: marshalGorm(meals),
	}, nil
}

func marshalGorm(meals []models.Meal) string {
	a, _ := json.Marshal(meals)
	n := len(a)
	s := string(a[:n])

	return s
}