data "aws_region" "current" {}

variable "subnets" {
  #type = list(string)
}


variable "security_group" {
  #type = string
}

variable "api_gateway" {
  
}

variable db_host {

}

variable "db_name" {
  
}

variable "db_user" {

}

variable "db_pass" {
  
}



data "archive_file" "insert_lambda_code" {
  type        = "zip"
  source_file = "${path.module}/src/bin/insert"
  output_path = "${path.module}/insert.zip"
  depends_on  = ["null_resource.build_lambda"]
}

data "archive_file" "list_lambda_code" {
  type        = "zip"
  source_file = "${path.module}/src/bin/list"
  output_path = "${path.module}/list.zip"
  depends_on  = ["null_resource.build_lambda"]
}

data "archive_file" "delete_lambda_code" {
  type        = "zip"
  source_file = "${path.module}/src/bin/delete"
  output_path = "${path.module}/delete.zip"
  depends_on  = ["null_resource.build_lambda"]
}

resource "null_resource" "build_lambda" {
  triggers = {
    build_number = "${timestamp()}"
  }

  provisioner "local-exec" {
    command     = " ${path.module}/build.sh"
    interpreter = ["bash", "-c"]
  }
}

resource "aws_lambda_function" "insert" {
  function_name    = "insert"
  handler          = "insert"
  filename         = "${data.archive_file.insert_lambda_code.output_path}"
  source_code_hash = "${data.archive_file.insert_lambda_code.output_base64sha256}"

  role        = "${aws_iam_role.lambdaRole.arn}"
  runtime     = "go1.x"
  memory_size = 128
  timeout     = 10

  vpc_config {
    subnet_ids         = "${var.subnets}"
    security_group_ids = ["${var.security_group.id}"]
  }

  environment {
    variables = {
      DB_HOST = "${var.db_host}"
      DB_NAME = "${var.db_name}"
      DB_USER = "${var.db_user}"
      DB_PASS = "${var.db_pass}"
      DB_PORT = "3306"
    }
  }
}

resource "aws_lambda_function" "list" {
  function_name    = "list"
  handler          = "list"
  filename         = "${data.archive_file.list_lambda_code.output_path}"
  source_code_hash = "${data.archive_file.list_lambda_code.output_base64sha256}"

  role        = "${aws_iam_role.lambdaRole.arn}"
  runtime     = "go1.x"
  memory_size = 128
  timeout     = 10

  vpc_config {
    subnet_ids         = "${var.subnets}"
    security_group_ids = ["${var.security_group.id}"]
  }

  environment {
    variables = {
      DB_HOST = "${var.db_host}"
      DB_NAME = "${var.db_name}"
      DB_USER = "${var.db_user}"
      DB_PASS = "${var.db_pass}"
      DB_PORT = "3306"
    }
  }
}

resource "aws_lambda_function" "delete" {
  function_name    = "delete"
  handler          = "delete"
  filename         = "${data.archive_file.delete_lambda_code.output_path}"
  source_code_hash = "${data.archive_file.delete_lambda_code.output_base64sha256}"

  role        = "${aws_iam_role.lambdaRole.arn}"
  runtime     = "go1.x"
  memory_size = 128
  timeout     = 10

  vpc_config {
    subnet_ids         = "${var.subnets}"
    security_group_ids = ["${var.security_group.id}"]
  }

  environment {
    variables = {
      DB_HOST = "${var.db_host}"
      DB_NAME = "${var.db_name}"
      DB_USER = "${var.db_user}"
      DB_PASS = "${var.db_pass}"
      DB_PORT = "3306"
    }
  }
}

resource "aws_iam_role" "lambdaRole" {

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": {
    "Action": "sts:AssumeRole",
    "Principal": {
      "Service": "lambda.amazonaws.com"
    },
    "Effect": "Allow"
  }
}
POLICY
}

resource "aws_iam_role_policy_attachment" "attach_lambdaVPC" {
  role = "${aws_iam_role.lambdaRole.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
  
}


# Allow API gateway to invoke the hello Lambda function.
resource "aws_lambda_permission" "insert" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.insert.arn}"
  principal     = "apigateway.amazonaws.com"
}

resource "aws_lambda_permission" "list" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.list.arn}"
  principal     = "apigateway.amazonaws.com"
}

# A Lambda function is not a usual public REST API. We need to use AWS API
# Gateway to map a Lambda function to an HTTP endpoint.
resource "aws_api_gateway_resource" "list" {
  rest_api_id = "${var.api_gateway.id}"
  parent_id   = "${var.api_gateway.root_resource_id}"
  path_part   = "list"
}

resource "aws_api_gateway_resource" "list_month" {
  rest_api_id = "${var.api_gateway.id}"
  parent_id = "${aws_api_gateway_resource.list.id}"
  path_part = "{month}"
}

resource "aws_api_gateway_resource" "list_year" {
  rest_api_id = "${var.api_gateway.id}"
  parent_id = "${aws_api_gateway_resource.list_month.id}"
  path_part = "{year}"
}


#           GET
# Internet -----> API Gateway
resource "aws_api_gateway_method" "list" {
  rest_api_id   = "${var.api_gateway.id}"
  resource_id   = "${aws_api_gateway_resource.list_year.id}"
  http_method   = "GET"
  authorization = "NONE"

  request_parameters = {
    "method.request.path.month" = true
    "method.request.path.year" = true
  }
}

resource "aws_api_gateway_resource" "insert" {
  rest_api_id = "${var.api_gateway.id}"
  parent_id   = "${var.api_gateway.root_resource_id}"
  path_part   = "insert"
}

#           GET
# Internet -----> API Gateway
resource "aws_api_gateway_method" "insert_post" {
  rest_api_id   = "${var.api_gateway.id}"
  resource_id   = "${aws_api_gateway_resource.insert.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_method" "insert_options" {
  rest_api_id   = "${var.api_gateway.id}"
  resource_id   = "${aws_api_gateway_resource.insert.id}"
  http_method   = "OPTIONS"
  authorization = "NONE"
}

resource "aws_api_gateway_method_response" "insert_options" {
  rest_api_id   = "${var.api_gateway.id}"
  resource_id   = "${aws_api_gateway_resource.insert.id}"
  http_method = "${aws_api_gateway_method.insert_options.http_method}"
  status_code = "200"
  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" = "true",
    "method.response.header.Access-Control-Allow-Methods" = "true",
    "method.response.header.Access-Control-Allow-Origin"  = "true"
  }
}

resource "aws_api_gateway_integration_response" "insert_options" {
  rest_api_id   = "${var.api_gateway.id}"
  resource_id   = "${aws_api_gateway_resource.insert.id}"
  http_method = "${aws_api_gateway_method.insert_options.http_method}"
  status_code = "${aws_api_gateway_method_response.insert_options.status_code}"

  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" = "'*'",
    "method.response.header.Access-Control-Allow-Methods" = "'GET,POST,OPTIONS'",
    "method.response.header.Access-Control-Allow-Origin"  = "'*'" 
  }
}

resource "aws_api_gateway_method" "delete" {
  rest_api_id   = "${var.api_gateway.id}"
  resource_id   = "${aws_api_gateway_resource.delete_year.id}"
  http_method   = "DELETE"
  authorization = "NONE"

    request_parameters = {
    "method.request.path.day" = true
    "method.request.path.month" = true
    "method.request.path.year" = true
  }
}

resource "aws_api_gateway_resource" "delete_day" {
  rest_api_id = "${var.api_gateway.id}"
  parent_id = "${aws_api_gateway_resource.insert.id}"
  path_part = "{day}"
}

resource "aws_api_gateway_resource" "delete_month" {
  rest_api_id = "${var.api_gateway.id}"
  parent_id = "${aws_api_gateway_resource.delete_day.id}"
  path_part = "{month}"
}

resource "aws_api_gateway_resource" "delete_year" {
  rest_api_id = "${var.api_gateway.id}"
  parent_id = "${aws_api_gateway_resource.delete_month.id}"
  path_part = "{year}"
}
#              POST
# API Gateway ------> Lambda
# For Lambda the method is always POST and the type is always AWS_PROXY.
#
# The date 2015-03-31 in the URI is just the version of AWS Lambda.
resource "aws_api_gateway_integration" "list" {
  rest_api_id             = "${var.api_gateway.id}"
  resource_id             = "${aws_api_gateway_resource.list_year.id}"
  http_method             = "${aws_api_gateway_method.list.http_method}"
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "arn:aws:apigateway:${data.aws_region.current.name}:lambda:path/2015-03-31/functions/${aws_lambda_function.list.arn}/invocations"

  request_parameters = {
    "integration.request.path.month" = "method.request.path.month"
    "integration.request.path.year" = "method.request.path.year"
  }
}

resource "aws_api_gateway_integration" "insert_post" {
  rest_api_id             = "${var.api_gateway.id}"
  resource_id             = "${aws_api_gateway_resource.insert.id}"
  http_method             = "${aws_api_gateway_method.insert_post.http_method}"
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "arn:aws:apigateway:${data.aws_region.current.name}:lambda:path/2015-03-31/functions/${aws_lambda_function.insert.arn}/invocations"
}

resource "aws_api_gateway_integration" "insert_options" {
  rest_api_id             = "${var.api_gateway.id}"
  resource_id             = "${aws_api_gateway_resource.insert.id}"
  http_method             = "${aws_api_gateway_method.insert_options.http_method}"
  type                    = "MOCK"
}

resource "aws_api_gateway_integration" "delete" {
  rest_api_id             = "${var.api_gateway.id}"
  resource_id             = "${aws_api_gateway_resource.delete_year.id}"
  http_method             = "${aws_api_gateway_method.delete.http_method}"
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "arn:aws:apigateway:${data.aws_region.current.name}:lambda:path/2015-03-31/functions/${aws_lambda_function.delete.arn}/invocations"

  request_parameters = {
    "integration.request.path.day" = "method.request.path.day"
    "integration.request.path.month" = "method.request.path.month"
    "integration.request.path.year" = "method.request.path.year"
  }
}

# This resource defines the URL of the API Gateway.
resource "aws_api_gateway_deployment" "api" {
  depends_on = [
    "aws_api_gateway_integration.insert_post",
    "aws_api_gateway_integration.insert_options",
    "aws_api_gateway_integration.list",
    "aws_api_gateway_integration.delete"
  ]

  rest_api_id = "${var.api_gateway.id}"
  stage_name  = "v1"
}

output "apiUrl" {
  value = "${aws_api_gateway_deployment.api.invoke_url}"
}


