
variable "apiUrl" {
  type = string
}

resource "aws_s3_bucket" "app-frontend" {
  
  website {
    index_document = "index.html"
  }
  
  provisioner "local-exec" {
    command = <<EOT
      echo export "const environment = {  
              apiUrl: ${var.apiUrl},
              debugMode: false
            };" || ${path.module}/app/src/environments/environment.prod.ts
EOT
  }

  provisioner "local-exec" {
    when = "destroy"
    command = "aws s3 rm s3://${self.id} --recursive"
  }
}

resource "null_resource" "build_bucket" {
  depends_on = [aws_s3_bucket.app-frontend]
  triggers = {
    build_number = "${timestamp()}"
  }
    provisioner "local-exec" {
    command = <<EOT
      cd ${path.module}/app && ng build --configuration=production   && 
      aws s3 cp dist/frontend s3://${aws_s3_bucket.app-frontend.id} --recursive
EOT
  }
}

resource "aws_s3_bucket_policy" "app-frontend-policy" {
    bucket = "${aws_s3_bucket.app-frontend.id}"

    policy = <<POLICY
{
  "Version":"2012-10-17",
  "Statement":[{
 	  "Sid":"PublicReadForGetBucketObjects",
    "Effect":"Allow",
 	  "Principal": "*",
      "Action":["s3:GetObject"],
      "Resource":["${aws_s3_bucket.app-frontend.arn}/*"]
  }]
}
POLICY
}


output "website-url" {
  value = "${aws_s3_bucket.app-frontend.website_endpoint}"
}

