import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { MonthViewComponent } from './month-view/month-view.component';
import { MealRowComponent } from './meal-row/meal-row.component';

@NgModule({
  declarations: [
    AppComponent,
    MonthViewComponent,
    MealRowComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
