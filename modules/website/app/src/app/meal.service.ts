import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import { HttpClient } from '@angular/common/http'
import { Meal } from './core/meal'
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})

export class MealService {
  private apiURL: string = 'https://n4vki9wa93.execute-api.eu-central-1.amazonaws.com/v1';

  constructor(private httpClient: HttpClient) { }

  getMeals(): Observable<Meal[]> {
    return this.httpClient.get<Meal[]>(this.apiURL + '/list/5/2019')
  }

  getColumns(): string[] {
    return ["date", "name", "price", "action"]
  };

  addMeal(meal: Meal): Observable<Meal> {
    const httpOptions = {
      headers: new HttpHeaders({
        //'Content-Type': 'application/json',
      })
    };
    return this.httpClient.post<Meal>(this.apiURL + '/insert', meal, httpOptions)
  }
}
