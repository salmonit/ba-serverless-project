import { Component, OnInit, Input } from '@angular/core';
import { Meal } from '../core/meal';
import { FormControl } from '@angular/forms';
import { MealService } from '../meal.service';


@Component({
  selector: '[app-meal-row]',
  templateUrl: './meal-row.component.html',
  styleUrls: ['./meal-row.component.styl']
})
export class MealRowComponent implements OnInit {

  mealName = new FormControl('');

  addMode: boolean = false;

  @Input() meal: Meal;
  @Input() columns: string[];

  constructor(private mealService: MealService) { }

  ngOnInit() {
  }

  addNewMode() {
    this.addMode = true
  }

  add(name,price) {
    this.addMode = !this.addMode
    this.mealService.addMeal({date: this.meal.date, name: name})
      .subscribe(meal => console.log(meal))
  }
}
